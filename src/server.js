"use strict";
require("dotenv").config();
//import { normalize, schema } from 'normalizr'
//const fs = require('fs')
//const app = require ('express')
const uuid = require("uuid");
const R = require("ramda");
const v = require("voca");
const PORT = 3000;
// Pure functions
const _typeof = (x) => typeof x;
const arrTypeOf = (o) => R.map(_typeof)(o);
const delObjFromArr = (id) => (a) => R.dissoc(id, a);
const addIdToObj = (o) => R.assoc("id", uuid.v4())(o);
const addTimeStamp = (o) => R.assoc("date", Date())(o);
const addIdAndDate = (o) => R.pipe(addTimeStamp, addIdToObj)(o);
const validateObj = (obj) => (objMOD) => {
    let a = normalize(obj);
    let b = normalize(objMOD);
    return R.equals(a)(b);
};
const normalize = (o) => {
    return R.objOf(R.keys(o), arrTypeOf(o));
};
const formatObj = (o) => {
    const id = R.pipe(addIdAndDate, R.prop("id"))(o);
    const x = addIdAndDate(o);
    return R.objOf(id, x);
};
const pushToVector = (o) => (v) => R.append(o)(v);
const returnCopy = (o) => (eo) => {
    let i = R.pick(["id"])(o);
    let xs = { [i.id]: o };
    return { ...eo, ...xs };
};
const formatAndPush = (o) => (ste) => R.pipe(addIdAndDate, returnCopy)(o)(ste);
const DataController = (o) => (oMod) => (storage) => (v) => {
    if (!validateObj(o)(oMod))
        return { _object: o, _vector: v };
    const newObj = formatAndPush(o)(storage);
    const newVector = pushToVector(newObj)(v);
    return { _object: newObj, _vector: newVector };
};
const ste = (v) => (R.empty(v) ? {} : R.last(v));
// Impure Nodes
const MODEL = {
    a: String(),
};
const TEST = {
    a: "asd",
};
// TODO move all this bull**** into their own thing
let vectorProductos = new Array(); // Only allowed push and prop
