require("dotenv").config();
//import { normalize, schema } from 'normalizr'
//const fs = require('fs')
//const app = require ('express')
const uuid = require("uuid");
const R = require("ramda");
const v = require("voca");
const PORT: number = 3000;

// Pure functions
const _typeof = (x: Readonly<unknown>) => typeof x;
const arrTypeOf = (o: Readonly<object>) => R.map(_typeof)(o);
const delObjFromArr = (id: Readonly<string>) => (a: object[]) =>
  R.dissoc(id, a);
const addIdToObj = (o: Readonly<object>) => R.assoc("id", uuid.v4())(o);
const addTimeStamp = (o: Readonly<object>) => R.assoc("date", Date())(o);
const addIdAndDate = (o: Readonly<object>) =>
  R.pipe(addTimeStamp, addIdToObj)(o);
const validateObj = (obj: Readonly<object>) => (objMOD: Readonly<object>) => {
  let a: object = normalize(obj);
  let b: object = normalize(objMOD);
  return R.equals(a)(b);
};
const normalize = (o: Readonly<object>) => {
  return R.objOf(R.keys(o), arrTypeOf(o));
};

const formatObj = (o: Readonly<object>) => {
  const id = R.pipe(addIdAndDate, R.prop("id"))(o);
  const x = addIdAndDate(o);
  return R.objOf(id, x);
};
// Interface
type formated = {
  id: string;
  date: string;
};
type nObject = {
  _object: object;
  _vector: object[];
};
const pushToVector = (o: Readonly<object>) => (v: Readonly<object[]>) =>
  R.append(o)(v);
const returnCopy = (o: Readonly<object>) => (eo: Readonly<object>) => {
  let i: formated = R.pick(["id"])(o);
  let xs = { [i.id]: o };
  return { ...eo, ...xs };
};
const formatAndPush = (o: Readonly<object>) => (ste: Readonly<object>) =>
  R.pipe(addIdAndDate, returnCopy)(o)(ste);
const DataController = (o: Readonly<object>) => (oMod: Readonly<object>) => (
  storage: Readonly<object>
) => (v: Readonly<object[]>) => {
  if (!validateObj(o)(oMod)) return { _object: o, _vector: v };
  const newObj: object = formatAndPush(o)(storage);
  const newVector: object[] = pushToVector(newObj)(v);
  return { _object: newObj, _vector: newVector };
};
const ste: object = (v: Readonly<object[]>) => (R.empty(v) ? {} : R.last(v));
// Impure Nodes
const MODEL: object = {
  a: String(),
};
const TEST: object = {
  a: "asd",
};
// TODO move all this bull**** into their own thing
let vectorProductos: object[] = new Array(); // Only allowed push and prop
